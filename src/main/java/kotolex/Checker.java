package kotolex;

import java.util.HashMap;
import java.util.Set;

public final class Checker {
    private static HashMap <String, Integer> checked = new HashMap<>();
    private final boolean allowRedirect;

    public Checker(boolean allowRedirect) {
        this.allowRedirect = allowRedirect;
    }

    public Checker() {
        allowRedirect = true;
    }

    public static Set<String> getChecked() {
        return checked.keySet();
    }

    public boolean isUrlAlive(String url) {
        int minCode = 200;
        int maxCode = allowRedirect ? 308 : 226;
        int respCode;
        if (checked.containsKey(url)){
            respCode = checked.get(url);
        }
        else {
            respCode = new HeadRequest(url).responseCode();
            synchronized (checked){
                checked.put(url, respCode);
            }
        }
        return respCode >= minCode && respCode <= maxCode;
    }
}
