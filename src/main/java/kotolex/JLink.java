package kotolex;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public final class JLink {
    private final String mainPath;
    private ConcurrentSkipListSet<String> needToVisit;
    private ConcurrentHashMap<String, String> brokenLinks;
    private List<String> visited;
    private ArrayList<Future> tasks;
    private int THREADS_COUNT = 20;
    private long elapsedTime = 0;
    private final String[] excludeTypes = {"gif", "jpg", "png", "js", "css", "xml", "zip", "txt", "java", "avi", "jar", "ico", "rar"};

    public JLink(String mainPath) {
        this.mainPath = mainPath;
        needToVisit = new ConcurrentSkipListSet<>();
        brokenLinks = new ConcurrentHashMap<>();
        visited = new ArrayList<>();
        needToVisit.add(mainPath);
    }

    public JLink(String mainPath, int THREADS_COUNT) {
        this(mainPath);
        this.THREADS_COUNT = THREADS_COUNT > 0 ? THREADS_COUNT : 20;
    }

    private boolean isInVisited(String url) {
        boolean result;
        synchronized (visited) {
            result = visited.contains(url);
        }
        return result;
    }

    private void addToVisited(String url) {
        synchronized (visited) {
            visited.add(url);
        }
    }

    private void init() {
        brokenLinks.clear();
        visited.clear();
        elapsedTime = 0;
    }

    public void start() {
        init();
        tasks = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        ExecutorService service = Executors.newFixedThreadPool(THREADS_COUNT);
        while (true) {
            if (!needToVisit.isEmpty()) {
                String newUrl = needToVisit.pollFirst();
                FutureTask task = new FutureTask(new Visitor(newUrl), true);
                service.submit(task);
                tasks.add(task);
            }
            Iterator<Future> iterator = tasks.iterator();
            while (iterator.hasNext()) {
                Future task = iterator.next();
                if (task.isDone()) {
                    iterator.remove();
                }
            }
            if (tasks.isEmpty() && needToVisit.isEmpty()) {
                System.out.println("Complete!");
                break;
            }
        }
        service.shutdownNow();
        elapsedTime = System.currentTimeMillis() - startTime;
    }

    public long getElapsedTime() {
        return elapsedTime;
    }

    public Map<String, String> getBrokenLinks() {
        return brokenLinks;
    }

    public List<String> getVisited() {
        return visited;
    }

    public Set<String> getChecked() {
        return Checker.getChecked();
    }

    class Visitor implements Runnable {
        private final String urlPath;

        public Visitor(String urlPath) {
            this.urlPath = urlPath;
            addToVisited(urlPath);
            System.out.println("Visit " + urlPath);
        }

        @Override
        public void run() {
            WebPage page = new WebPage(urlPath);
            List<String> all = page.getAllLinks();
            Checker checker = new Checker();
            Map<Boolean, List<String>> both = all.stream().
                    collect(Collectors.partitioningBy(Checker.getChecked()::contains)).get(Boolean.FALSE).
                    parallelStream().
                    collect(Collectors.partitioningBy(checker::isUrlAlive));
            for (String link : both.get(Boolean.FALSE)) {
                brokenLinks.put(link, urlPath);
            }
            both.get(Boolean.TRUE).parallelStream().
                    filter(n -> n.startsWith(mainPath)).
                    filter(n -> !isInExcludedList(n)).
                    filter(n -> !isInVisited(n)).
                    forEach(needToVisit::add);
        }

        public boolean isInExcludedList(String url) {
            for (String exc : excludeTypes) {
                if (url.endsWith(exc)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static void main(String[] args) {
        JLink jLink = new JLink("http://skipy.ru");
        jLink.start();
        System.out.println(jLink.elapsedTime);
        System.out.println(jLink.brokenLinks.size());
    }
}
