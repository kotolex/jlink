package kotolex;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public final class HeadRequest {
    private final String url;
    private int responseCode = 0;

    public HeadRequest(String url) {
        this.url = url;
    }

    public int responseCode() {
        if (responseCode != 0) {
            return responseCode;
        }
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");
            connection.setInstanceFollowRedirects(true);
            connection.connect();
            responseCode = connection.getResponseCode();
            return responseCode;
        } catch (IOException e) {
            return 0;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
