package kotolex;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class WebPage {
    private final String urlPath;
    private Document document;

    public WebPage(String urlPath) {
        this.urlPath = urlPath;
    }

    public List<String> getAllLinks() {
        List<String> hrefs = getLinksHrefs();
        hrefs.addAll(getImagesAndScriptsSources());
        return hrefs;
    }

    public List<String> getImagesAndScriptsSources() {
        return getListByLocatorAndAttribute("[src]", "src");
    }

    public List<String> getLinksHrefs() {
        return getListByLocatorAndAttribute("a[href]", "href");
    }

    private List<String> getListByLocatorAndAttribute(String locator, String attr) {
        if (get() != null) {
            return document.select(locator).stream().map((n) -> n.attr(attr)).map(this::formUrl).filter((n) -> !n.isEmpty()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private String formUrl(String url) {
        if (url.startsWith("mailto")) {
            return "";
        }
        if (!url.startsWith("http")) {
            try {
                url = new URL(new URL(urlPath), url).toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return "";
            }
        }
        return url.contains("#") ? url.split("#")[0] : url;
    }

    private Document get() {
        if (document == null) {
            try {
                document = Jsoup.connect(urlPath).get();
            } catch (IOException e) {
                e.printStackTrace();
                document = null;
            }
        }
        return document;
    }
}
